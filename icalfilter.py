import icalendar
import datetime
import pytz
utc = pytz.utc

date_from = datetime.datetime.now()
date_till = datetime.datetime(2023, 12, 24)

stream_classes = ["025", "030", "034", "038", "133", "136", "140"]

def main():
    with open('in.ics', 'r', encoding='utf-8') as f:
        cal = icalendar.Calendar.from_ical(f.read())
        outcal = icalendar.Calendar()

        for name, value in cal.items():
            outcal.add(name, value)

        def active_event(item):
            if any(stream_class in (item["location"]).strip() for stream_class in stream_classes):
                return True
            else:
                return False
            raise Exception('ARGH')


        for item in cal.subcomponents:
            if item.name == 'VEVENT':
                start_date = item['dtstart'].dt
                if active_event(item):
                    print ('INCLUDE', item['summary'], repr(start_date), item['location'])
                    outcal.add_component(item)
                else:
                    #print ('EXCLUDE', item['summary'], repr(start_date), item['location'])
                    pass
            else:
                outcal.add_component(item)

        with open('out.ics', 'wb') as outf:
            outf.write(outcal.to_ical(sorted=False))

main()