import icalendar
import datetime
import pytz
from icalendar import vText
utc = pytz.utc

date_from = datetime.datetime.now()
date_till = datetime.datetime(2023, 12, 24)

def main():
    with open('in.ics', 'r', encoding='utf-8') as f:
        cal = icalendar.Calendar.from_ical(f.read())
        outcal = icalendar.Calendar()

        for name, value in cal.items():
            outcal.add(name, value)

        def active_event(item):
            if (item["location"] == ""):
                return False
            start_date = item['dtstart'].dt

            # recurrent
            if 'RRULE' in item:
                rrule = item['RRULE']
                # print (rrule)
                if 'UNTIL' not in rrule:
                    return False
                else:
                    assert len(rrule['UNTIL']) == 1
                    until_date = rrule['UNTIL'][0]

                    if type(until_date) == datetime.datetime:
                        return (until_date >= utc.localize(date_from) and until_date <= utc.localize(date_till)) 

                    if type(until_date) == datetime.date:
                        return False #until_date >= datetime.date(2019, 1, 1)

                    raise Exception('Unknown date format for "UNTIL" field')

            # not reccurrent
            if type(start_date) == datetime.datetime:
                return (start_date >= utc.localize(date_from) and start_date <= utc.localize(date_till)) 

            if type(start_date) == datetime.date:
                return False #start_date >= datetime.date(2019, 1, 1)

            raise Exception('ARGH')


        for item in cal.subcomponents:
            if item.name == 'VEVENT':
                start_date = item['dtstart'].dt
                if active_event(item):
                    print ('INCLUDE', item['summary'], repr(start_date), item['location'])
                    outcal.add_component(item)
                else:
                    #print ('EXCLUDE', item['summary'], repr(start_date), item['location'])
                    pass
            else:
                outcal.add_component(item)

        with open('out.ics', 'wb') as outf:
            outf.write(outcal.to_ical(sorted=False))

main()